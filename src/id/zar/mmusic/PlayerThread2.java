/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.mmusic;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

/**
 *
 * @author mozar
 */
public class PlayerThread2 extends Application {

    private MediaPlayer mplayer;

    public PlayerThread2() {
    }
    private String pathMusik = "";

    public void putar(String path) {
        this.pathMusik = path;
        launch(path);
    }

    @Override
    public void start(Stage stage) throws Exception {
        File f = new File(pathMusik);
        String p = f.toURI().toString();
        mplayer= new MediaPlayer(new Media(p));
        mplayer.play();
    }
}
