/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.mmusic;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import org.mp3transform.Decoder;

/**
 *
 * @author mozar
 */
public class PlayerThread extends Thread {

  private int buffer = 128 * 1024;
  private Decoder decoder = new Decoder();

  @Override
  public void run() {
    if (myFile == null) {
      System.out.println("file belum di tentukan");
      return;
    }
    try {
      FileInputStream fis = new FileInputStream(myFile);
      BufferedInputStream bis = new BufferedInputStream(fis, buffer);
      decoder.stop();
      System.out.println("set volume");
      decoder.setVolume(90);
      decoder.play(myFile.getAbsolutePath(), bis);
      
    } catch (Exception x) {
      x.printStackTrace();
    }
  }
  private File myFile = null;

  public void playStart(String path) {
    File f = new File(path);
    if (f.exists()) {
      myFile = f;
      this.start();
    } else {
      System.out.println("file " + path + " tidak tersedia");
    }
  }

  public void playPause() {
    decoder.pause();
  }

  public void playStop() {
    decoder.stop();
  }
}
